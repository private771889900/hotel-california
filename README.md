Hello!

Authors: Enrico Richiello & Erik Björkqvist

The hotel owners seeks a database system to centralize and streamline the Nordic Haven Hotel Chain operations including reservations, guest services, and departmental coordination for the employees.

The system allows employees to see real-time updates on bookings and access to operational data across the hotel and possibly future hotels. Accounting for requested seamless communication between departments and scalability.

Guest profiles, booking details, booking history, booking availability, loyalty programs and seasonal rates, allow for data analatytics to understand guest trends and personalized service preferences.

A conceptual, logical and physical ERD is located in file 'Nordic Haven Hotel Chain ERD.drawio'.

Explanation of Booking_History:

The Booking_History table collects and saves information about each reservation made in the system. We selected
to save info that we think will be useful in data analytics and to analyse customer behaviour. If one reservation
contains multiple rooms/venues/amenities/tables, a separate row will be created in the Booking_History table for each entity. While this may not be the most efficient solution, it works for the intended purpose. 

The Booking_History table is necessary since the Reservations table will be cleaned and expired reservations will be removed continuously (by for example a procedure) to keep the Reservations table up to date and easier to navigate.

Differences between the Conceptual, Logical and Physical ERD:

There are some differences between each ERD, with the Physical one being the final product. As the work progressed,
we realized that the conceptual design had some logical issues and limitations and therefore changed the schema slightly. The conceptual and logical design went through many iterations, but we still found issues with these designs
when we created the physical ERD. This truly proves how difficult it is to create a perfect design from the beginning!